// NOTE: Color string cannot include '#' because of URL encoding!
const hexRegex = /^#?([a-f0-9]{6})$/i;

/**
 * Get a contrasting text color
 * Taken from: https://gomakethings.com/dynamically-changing-the-text-color-based-on-background-color-contrast-with-vanilla-js/
 * @param  {string} hexColor - Hex color string
 * @return {string}          - Contrasting text color
 */
const getContrastText = (hexColor) => {
  if (!hexColor) return "black";

  // Remove leading '#' if provided
  if (hexColor.slice(0, 1) === "#") {
    hexColor = hexColor.slice(1);
  }

  // Invalid hex colors should use black text
  if (!hexRegex.test(hexColor)) return "black";

  const r = parseInt(hexColor.substr(0, 2), 16);
  const g = parseInt(hexColor.substr(2, 2), 16);
  const b = parseInt(hexColor.substr(4, 2), 16);

  const yiq = (r * 299 + g * 587 + b * 114) / 1000;
  return yiq >= 128 ? "black" : "white";
};

/**
 * Parse a hex color string
 * @param  {string} colorString - Hex color string
 * @return {string}             - Parsed hex color
 */
const parseHexColor = (colorString) => {
  if (!colorString) return null;

  const match = colorString.match(hexRegex);
  return match ? match[1] : null;
};

export { getContrastText, parseHexColor };
