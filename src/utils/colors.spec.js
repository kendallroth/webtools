// Utilities
import { getContrastText, parseHexColor } from "./colors";

describe("Get Contrasting Text", () => {
  it("should get contrasting text - black", () => {
    const contrastColor = getContrastText("39B631");
    const contrastColorBash = getContrastText("#39B631");

    expect(contrastColor).toEqual("black");
    expect(contrastColorBash).toEqual("black");
  });

  it("should get contrasting text - white", () => {
    const contrastColor = getContrastText("4117FA");
    const contrastColorBash = getContrastText("#4117FA");

    expect(contrastColor).toEqual("white");
    expect(contrastColorBash).toEqual("white");
  });

  it("should handle invalid color", () => {
    const invalidColor = getContrastText("thisis");
    const extraColorChars = getContrastText("dedede2");

    expect(invalidColor).toEqual("black");
    expect(extraColorChars).toEqual("black");
  });

  it("should handle missing color arg", () => {
    const emptyColor = getContrastText("");

    expect(emptyColor).toEqual("black");
  });
});

describe("Parse Hex Colors", () => {
  it("should parse valid hex string", () => {
    const colorString = "d4d4d4";
    const colorStringBash = "#d4d4d4";

    const color = parseHexColor(colorString);
    const colorBash = parseHexColor(colorStringBash);

    expect(color).toEqual(colorString);
    expect(colorBash).toEqual(colorString);
  });

  it("should not parse invalid hex string", () => {
    const invalidColor = parseHexColor("thisis");
    const extraColorChars = parseHexColor("dedede2");

    expect(invalidColor).toEqual(null);
    expect(extraColorChars).toEqual(null);
  });

  it("should not parse invalid hex string args", () => {
    const emptyColor = parseHexColor();

    expect(emptyColor).toEqual(null);
  });
});
