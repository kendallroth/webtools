import { createApp } from "vue";

// Components
import App from "./App";

// Utilities
import router from "./router";
import store from "@store";

// Plugins
import ComponentsPlugin from "@plugins/components";

// Styles
import "@styles/app.scss";
require("typeface-muli");

import "./registerServiceWorker";

createApp(App)
  // Vue options
  .use(store)
  .use(router)
  // Plugins
  .use(ComponentsPlugin)
  .mount("#app");
