import { createRouter, createWebHistory } from "vue-router";

// Components
import RandomColor from "@views/Random/RandomColor";
import Home from "@views/Home";

/**
 * Prefix routes with a path
 * @param {string}   prefix - Route prefix
 * @param {Object[]} routes - Route list
 */
const withPrefix = (prefix, routes) =>
  routes.map((route) => {
    route.path = prefix + route.path;
    return route;
  });

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  ...withPrefix("/random", [
    {
      path: "/color/:color?",
      name: "randomColor",
      component: RandomColor,
    },
  ]),
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
