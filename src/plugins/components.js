// Components
import ToolHeader from "@components/ToolHeader";

export default {
  install: (app) => {
    app.component(ToolHeader.name, ToolHeader);
  },
};
