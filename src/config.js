const config = {
  colors: {
    default: "#0064aa",
    redFaded: "#BB4861",
    maroonDark: "#6A2B45",
    yellowDark: "#D0C150",
    pinkDark: "#931750",
    purpleDeep: "#380F60",
  },
};

export default config;
