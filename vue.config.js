const path = require("path");

const src = path.resolve(__dirname, "src");

module.exports = {
  lintOnSave: false,

  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        prependData: `
          @import "~@styles/_mixins.scss";
        `,
      },
    },
  },

  configureWebpack: {
    resolve: {
      alias: {
        "@": src,
        "@assets": path.join(src, "assets"),
        "@components": path.join(src, "components"),
        "@config": path.join(src, "config"),
        "@hooks": path.join(src, "hooks"),
        "@plugins": path.join(src, "plugins"),
        "@store": path.join(src, "store"),
        "@styles": path.join(src, "styles"),
        "@utils": path.join(src, "utils"),
        "@views": path.join(src, "views"),
      },
    },
  },

  pwa: {
    name: 'WebTools',
    themeColor: '#BB4861'
  }
};
